#/bin/sh
set -e

FSET=$1
FDIR="dist/lunar-$FSET/TTF"

printf "Finding fonts in: %s\n" $FDIR

find $FDIR -type f \
     \( -iname '*.otf' -o -iname '*.ttf' -o -iname '*.woff' -o -iname '*.eot' -o -iname '*.ttc' \) \
     -print0 \
    | parallel --verbose --null --jobs=4 fontforge -script /nerd/font-patcher -out $FDIR -q --no-progressbars --complete --makegroups 6 {}

if [ -f font-patcher-log.txt ]; then
	cp -f font-patcher-log.txt $FDIR
fi

exit 0
